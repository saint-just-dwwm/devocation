# BIENVENUE DANS LA DOCUMENTATION D'INSTALLATION DU JEU DEVOCATION

Documentation d'installation :

1. Installation des commandes CLI Git

- https://git-scm.com/downloads Télécharger GIT 
- Accepter les termes généraux de la licence 
- Selectionner l'emplacement du fichier (laisser par défaut)
- Selectionner tous les composants 
- Ajoutez le raccopurci gitbash du menu démarrer par défaut 
- Choisissez votre editeur de texte par défaut (Visual Studio ?)
- Laissez git décider
- Installer git aussi sur les applications tierces 
- Utiliser open SSL pour le transport HTTPS 
- Laissez Windows Style
    
2. Réccupération des sources depuis le serveur

- Placez vous dans le dossier ou vous souhaitez réccupérer le projet
- git clone http://gitlab.com/saint-just-dwwm/devocation (Reccupére le dossier)
- cd devocation (Se placer dans le dossier du projet)
- git branch prenom
- git commit -m "YOUHOU J'AI MA BRANCHE"
- git push 
- git checkout prenom (Maintenant vous travaillez sur votre branche).


3. Lancement du projet 


4. Règles du jeu


